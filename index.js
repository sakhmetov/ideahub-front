// NAVIGATION
const hamburger = document.querySelector('.navigation__hamburger');
const navigation = document.querySelector('.navigation');
const links = document.querySelectorAll('.navigation__item');
const metaThemeColor = document.querySelector("meta[name=theme-color]");

hamburger.addEventListener('click', () => {
  navigation.classList.toggle('open');
  metaThemeColor.getAttribute('content') === '#27216a'
    ? metaThemeColor.setAttribute('content', '#000')
    : metaThemeColor.setAttribute('content', '#27216a');
});

links.forEach((link) => {
  link.addEventListener('click', () => {
    navigation.classList.toggle('open');
    metaThemeColor.getAttribute('content') === '#27216a'
    ? metaThemeColor.setAttribute('content', '#000')
    : metaThemeColor.setAttribute('content', '#27216a');
  });
});

// When the user scrolls down 50px from the top of the document, resize the header's font size
window.onscroll = function() {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
    navigation.classList.add('scrolling');
  } else {
    navigation.classList.remove('scrolling');
  }
}

// CONTACT FORM
const $form = $('form#contact-form'),
    url = 'https://script.google.com/macros/s/AKfycbyU7b7VN8CUJg_Pm8olP2uZ9c7H6y7KZDfwoOMwG3_XSfU05U37/exec'

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

$('#submit-form').on('click', function(e) {
  e.preventDefault();
  const jqxhr = $.ajax({
    url: url,
    type: "GET",
    dataType: "json",
    data: $form.serializeObject(),
    success: function (n) {
      $form.trigger("reset");
      window.location.href = '/thank-you.html';
    },
  });
})